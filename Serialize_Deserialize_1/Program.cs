﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Serialize_Deserialize_1
{
    public class Program
    {
        static void Main(string[] args)
        {
            //lets do some serialization from object to xml
            xml_serialize_1();

            //lets do some serialization from object to binary
            xml_serialize_2();

            Console.WriteLine("The way I see it, if you're gonna build a time machine into a car, why not do it with some style?");
            Console.WriteLine("That's all folks!");

            Console.ReadLine();
        }

        //this will also do what xml_serialize_1 does
        //but this one serailizes to binary
        private static void xml_serialize_2()
        {
            //first lets create the water bottle object

            Water_Bottle temp_bottle = new Water_Bottle
            {
                name_of_bottle = "bottle 2",
                color_of_bottle = "red"
            };

            //now let me get the serialization object
            IFormatter formatting_object_1 = new BinaryFormatter();
            //now, writting the serialized data into a file
            using (Stream stream_object_1 = new FileStream("temp_data.bin", FileMode.Create))
            {
                //just like in the other method, right side is the object
                //left side is the stream that will contain the serialized binary form of the object
                formatting_object_1.Serialize(stream_object_1, temp_bottle);
            }

            //at this point, you can go to the bin folder of this solution and see that the file temp_data.bin would be created
            //now lets do deserialization and get our original object back

            using (Stream stream_object_2 = new FileStream("temp_data.bin", FileMode.Open))
            {
                Water_Bottle temp_bottle_2 = (Water_Bottle)formatting_object_1.Deserialize(stream_object_2);
            }

            Console.WriteLine("here is the object contents, after it has been deserialized from its binary form");
            Console.WriteLine(" water bottle name is {0} and color is {1} ", temp_bottle.name_of_bottle, temp_bottle.color_of_bottle);
        }

        private static void xml_serialize_1()
        {
            //creating a serialization object of type Water_Bottle
            XmlSerializer object_serializer_1 = new XmlSerializer(typeof(Water_Bottle));

            string xml_string_1 = "";

            using (StringWriter write_string_object_1 = new StringWriter())
            {
                //creating an instance of water bottle and initialiazing it with some values
                Water_Bottle temp_bottle = new Water_Bottle
                {
                    name_of_bottle = "bottle 1",
                    color_of_bottle = "blue"
                };

                //now serializing it and adding it into the string.
                //on the left side, the string stream that will hold the serialized data
                //on the right side, the object that needs to be serialized
                object_serializer_1.Serialize(write_string_object_1, temp_bottle);
                xml_string_1 = write_string_object_1.ToString();
            }

            //now, I have the xml equivalent of the object
            //displaying it
            Console.WriteLine("here is the serialized object in xml");
            Console.WriteLine(xml_string_1);

            //now lets deserialize the data from the xml string and back into the object
            //this would complete the process

            //reading the xml string data into a string reading object
            using (StringReader read_string_object_1 = new StringReader(xml_string_1))
            {
                Water_Bottle temp_bottle = (Water_Bottle)object_serializer_1.Deserialize(read_string_object_1);

                //alright, at this point, I have obtained the deseriazed object
                //let me display the data from the object itself
                Console.WriteLine("Here is data from the deserialized xml and now in an object, data");
                Console.WriteLine("name of bottle - {0} color of bottle - {1} ", temp_bottle.name_of_bottle, temp_bottle.color_of_bottle);
            }
        }
    }

    //this is the class that I will use for serialization and deserialization
    //by using this attribute, I am making this class serializable
    [Serializable]
    public class Water_Bottle
    {
        public string name_of_bottle { get; set; }
        public string color_of_bottle { get; set; }
    }
}
